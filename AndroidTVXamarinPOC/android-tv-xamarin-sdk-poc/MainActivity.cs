﻿using Android.App;
using Android.Widget;
using Android.OS;
using Square.Picasso;
using static Android.Views.View;
using Android.Views;
using System;
using System.Threading.Tasks;
using System.Net.Http;
using Android.Content;
using System.Dynamic;
using Newtonsoft.Json;
using Microsoft.CSharp.RuntimeBinder;
using Newtonsoft.Json.Linq;

namespace android_tv_xamarin_sdk_poc
{
    [Activity(Label = "android_tv_xamarin_poc", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            this.RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);
            
            this.LoadIcon();
            this.SetIP();
            this.ConfigureButton();
        }


           private async void SetIP()
           {
                string url = "https://ipinfo.io/";

                var client = new HttpClient();
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Add("Accept", "application/json");

                HttpResponseMessage result = await client.GetAsync(url);
                var jsonString = result.Content.ReadAsStringAsync().Result;
                
                TextView ipView = FindViewById<TextView>(Resource.Id.ipTextView);
                JObject json = JObject.Parse(jsonString);
                ipView.SetText(String.Format("IP ({0})", json["ip"].ToString()),TextView.BufferType.Normal);
        }


        private void LoadIcon()
        {
            ImageView logoView = FindViewById<ImageView>(Resource.Id.logoView);
            Picasso.With(this).Load("http://brandmark.io/logo-rank/random/apple.png").Into(logoView);
        }

        public void setTitle(string title)
        {
            TextView ip = FindViewById<TextView>(Resource.Id.ipTextView);
            ip.SetText(title, TextView.BufferType.Normal);
        }

        private void ConfigureButton()
        {
            Button clickButton = FindViewById<Button>(Resource.Id.playVideoButton);
            clickButton.Click += (sender, e) => {
                View v = sender as View;
                Intent myIntent = new Intent(v.Context, typeof(PlayVideoActivity));
                v.Context.StartActivity(myIntent);
            };
        }   
    }
}

