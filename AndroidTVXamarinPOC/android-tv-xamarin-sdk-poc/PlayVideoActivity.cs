﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace android_tv_xamarin_sdk_poc
{
    [Activity(Label = "PlayVideoActivity")]
    public class PlayVideoActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            this.RequestWindowFeature(WindowFeatures.NoTitle);
            SetContentView(Resource.Layout.PlayVideo);

             VideoView videoView = FindViewById<VideoView>(Resource.Id.fullscreenVideoView);
             videoView.SetVideoPath("http://cds.y5w8j4a9.hwcdn.net/zlivingusa/index.m3u8");

             BackMediaController mediaController = new BackMediaController(this);
             mediaController.SetAnchorView(videoView);
             videoView.SetMediaController(mediaController);

            DisplayMetrics metrics = new DisplayMetrics();
            this.WindowManager.DefaultDisplay.GetMetrics(metrics);

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)videoView.LayoutParameters;
            layoutParams.Width = metrics.WidthPixels;
            layoutParams.Height = metrics.HeightPixels;
            layoutParams.LeftMargin = 0;
            videoView.LayoutParameters = layoutParams;
            videoView.Start();
        }
    }
}